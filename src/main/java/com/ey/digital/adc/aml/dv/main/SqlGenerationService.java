package com.ey.digital.adc.aml.dv.main;

import java.util.List;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class SqlGenerationService {

	public void generateDVSql(AbstractApplicationContext context) {
		String FILENAME = ""; // TODO: specify file name
		JdbcTemplate sqlJdbcTemplate = (JdbcTemplate) context.getBean("sqlJbdctemplate");
		BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME));

		// ib_tables contains a list of table names
		String getListofTables = "select table_name from ib_tables";
		List<String> tablesList = sqlJdbcTemplate.queryForList(getListofTables, String.class);

		// TODO create 'ib_tables_l2_dv' or make sure it exists
		// Query to update 'ib_tables_l2_dv'
		StringBuffer updateResTable = new StringBuffer();

		// hiveSqlNum is a SQL query with numeric data validation test
		StringBuffer hiveSqlNum = new StringBuffer();

		// hiveSqlNum is a SQL query with numeric data validation test
		StringBuffer hiveSqlDate = new StringBuffer();

		// hiveSqlNum is a SQL query with categorical data validation test
		StringBuffer hiveSqlCat = new StringBuffer();

		tablesList.forEach(table -> {
			// SQL statements to get list of numeric and categoric columns
			String getListofNumericColumns = "select column from "+table+" where column_type = 'numeric'";
			String getListofDateColumns = "select column from "+table+" where column_type = 'date'";
			String getListofNonNumericColumns = "select column from "+table+" where column_type = 'non_numeric'";

			// Execute SQL queries to get result sets
			List<String> numericColumnsList = sqlJdbcTemplate.queryForList(getListofNumericColumns, String.class);
			List<String> dateColumnsList = sqlJdbcTemplate.queryForList(getListofDateColumns, String.class);
			List<String> categoricColumnsList = sqlJdbcTemplate.queryForList(getListofNonNumericColumns, String.class);

			// Create hiveSqlNum string for data validation of numerical data
			hiveSqlNum.append("INSERT INTO ${hiveconf:"+table+"}_numeric SELECT ");
			numericColumnsList.forEach(column -> {
				hiveSqlNum.append("${hiveconf:"+column+"},"
						+ "(sum(if(isnull(${hiveconf:"+column+"})=true,1,0))/count(*))*100 as Perctage_Null_Value,"
						+ "case when sum (case when ${hiveconf:"+column+"} =null then 1 else 0 end ) > 0 then 'NULL CHECK FAIL'"
						+ "when sum (case when cast (${hiveconf:"+column+"} as double)=null then 1 else 0 end) > 0 then 'Numeric Check Fail'"
						+ "end as Validation_results"
						// TODO add more numerical data validation tests
						);
			});
			hiveSqlNum.append(" from "+table
					+ "groub by ${hiveconf:"+column+"};"); // end of hiveSqlNum query

			// Create hiveSqlDate string for data validation of date data
			hiveSqlDate.append("INSERT INTO ${hiveconf:"+table+"}_date SELECT ");
			dateColumnsList.forEach(column -> {
				hiveSqlDate.append("count(distinct(${hiveconf:"+column+"})) as Distinct_count,"
						+ "(sum(if(isnull(${hiveconf:"+column+"})=true,1,0))/count(*))*100 as Perctage_Null_Value,"
						+ "case when cast(${hiveconf:"+column+"} as date) = NULL then 'incorrect data type of date column'"
						+ "when sum (case when cast(${hiveconf:"+column+"} as date) < '01.01.2011' then 1 else 0  end) > 0 then 'Transaction date before evaluation period'"
						+ "when sum (case when cast(${hiveconf:"+column+"} as date) > '12.31.2015' then 1 else 0 end ) > 0 then 'Transaction date after evaluation period'"
						+ "when sum (case when (${hiveconf:"+column+"} = NULL) then 1 else 0 end) > 0 then 'Null check Fail'"
						+ "else 'Date validation Pass'"
						+ "end as Validation_results,"
						// TODO add more numerical data validation tests
						);
			});
			hiveSqlDate.append(" from "+table +";");

			// Create hiveSqlCat string for data validation of categorical data
			hiveSqlCat.append("INSERT INTO ${hiveconf:"+table+"}_non_numeric SELECT ");
			categoricColumnsList.forEach(column -> {
				hiveSqlCat.append("select case when (cast(${hiveconf:"+column+"} as double)=NULL) then 'Character check Fail' else 'Character check Pass' end as Results,"
						+ "case when sum ( case when ${hiveconf:"+column+"} rlike '[^a-zA-Z\\d\\s:]' then 1 else 0 end)  > 0 then 'Special Char check fail' end as Validation_Results"
						// TODO add more numerical data validation tests
						);
			});
			hiveSqlCat.append(" from "+table +";");

			/**
			* Insert hiveSqlNum and hiveSqlCat into SQL table 'ib_tables_l2_dv'
			* See TODO above for table creation
			*/
			// updateResTable.append("INSERT INTO ib_tables_l2_dv VALUES "+table+", "+hiveSqlNum+", "+hiveSqlCat);
			// int res = sqlJdbcTemplate.update(updateResTable);

			// Write queries to file
			bw.write("INSERT INTO ib_tables_l2_dv VALUES "+table+", "+hiveSqlNum+", "+hiveSqlDate+", "+hiveSqlCat);

			// Reset query strings
			hiveSqlNum.setLength(0);
			hiveSqlCat.setLength(0);
			hiveSqlDate.setLength(0);

		});
		bw.close();
	}
}
