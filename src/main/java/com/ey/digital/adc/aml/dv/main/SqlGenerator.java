package com.ey.digital.adc.aml.dv.main;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class SqlGenerator {

	private static final Logger log = Logger.getLogger(SqlGenerator.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// generateDVSQL("tran_amt")
		AbstractApplicationContext context = new ClassPathXmlApplicationContext(
				"/META-INF/spring/exec-engine-context.xml", SqlGenerator.class);
		log.info("Starting DV script creation");
		context.registerShutdownHook();
		generateDVScript(context);
		context.close();
		log.info("DV script creation finished");

	}

	public static void generateDVScript(AbstractApplicationContext context) {
		SqlGenerationService sqlGenerationService = new SqlGenerationService();
		sqlGenerationService.generateDVSql(context);
	}

}
